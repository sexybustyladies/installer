#!/bin/bash - 

DFDIR=${HOME}/.dotfiles
WD=`dirname $0`

get_rep (){
    cd $DFDIR || exit 1
    for REP in $1
    do
        git clone https://bitbucket.org/benfelin/${REP}.git $REP
        cd $REP
        echo $WD
        perl ${WD}/install.pl
        cd ..
    done
}

main (){
    DIALOG=dialog

    $DIALOG --backtitle "Dotfiles Manager" \
            --title "Welcome" --clear \
            --yesno "NEVER rewrite your dot files" 10 30

    case $? in
            0)	    echo "You know what's good"
                    ;;
            1)	    echo "You'll be back" && exit 0
                    ;;
            255)    echo "Boooooo" && exit 1
                    ;;
    esac

    fichtmp=$(tempfile 2>/dev/null) || fichtmp=/tmp/test$$ \
    trap "rm -f $fichtmp" 0 1 2 5 15

    $DIALOG --backtitle "Dotfiles Manager" \
            --title "Package Selector" --clear \
            --radiolist "What to do?  " 20 60 15  \
            "Install" "git-clone" on\
            "Update" "git-pull" off\
            "Delete" "delete repo and symlinks" off 2>$fichtmp
    $action=`cat $fichtmp`
    

    $DIALOG --backtitle "Dotfiles Manager" \
            --title "Package Selector" --clear \
            --checklist "Select your packages  " 20 60 15  \
            "git" "Your favorite VCS" ON\
            "vim" "Best editor" ON\
            "prog" "Bash, perl, etc" ON\
            "tmux" "Powerful multiplexer" ON\
            "file" "File Managers" ON\
            "tool" "SSH config and other treats" ON\
            "web" "ELinks and pals" off\
            "retro" "Gaming you know?" off\
            "media" "Audio & Video" off\
            "irc" "IRC will never die" off\
            "mail" "Mutt for connoisseurs" off\
            "window" "Window Managers :-)" off 2>$fichtmp
    valret=$?
    choix=`cat $fichtmp`

    case $valret in
    0)
                case $action in
                    inst)
                            if [[ ! -d $DFDIR  ]]; then mkdir $DFDIR; fi
                            get_rep "$choix"
                        ;;
                    updt)
                            up_rep "$choix"
                        ;;
                    remv)
                            del_rep "$choix"
                        ;;
                    
                esac
                ;;

    1)	        echo "You'll be back" && exit 0
                ;;
    255)	echo "Boooooo" && exit 1
                ;;
    esac
}

main

