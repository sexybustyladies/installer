#!/usr/bin/env perl

use 5.012;
use warnings;
use Carp;
use Cwd qw(cwd);
use File::Path qw(make_path);
use File::Which;
use File::Copy;
use YAML::XS 'LoadFile';

my $config = LoadFile('config.yaml');

# Go through application(s)
foreach my $app ( @{$config} ) {

    my ( $app_name, $app_desc, $app_cmd ) = (
        $app->{application}->{name},
        $app->{application}->{desc},
        $app->{application}->{command}
    );

    # It is already installed
    if ( which("$app_cmd") ) {
        say "Setting  " . $app_name . " - " . $app_desc;

        # Go through conf file(s)
        foreach my $f ( @{ $app->{application}->{file} } ) {

            my ( $src, $dir, $lnk ) =
                ( $f->{name}, $f->{dir}, $f->{link} );

            my $dest = $ENV{HOME};

            #1-That conf file isn't located in $HOME
            if ($dir) {
                $dest .= "/" . $dir;

                # Create its location if not existing
                unless ( -d $dest ) {
                    make_path($dest) or croak "mkdir failed: $!";
                }
            }

            #2-Make a copy of the conf file
            #  or delete it if symlink
            my $conf = $dest . "/" . $lnk;
            if ( !-l $conf ) {
                my $bck = $conf . ".bck";
                copy( $conf, $bck ) or croak "Copy failed: $!";
                say "BCK: " . $conf . " - " . $bck;
            }
            else {
                unlink($conf) or croak "Unlink failed :$!";
            }

            #3-New Symlink
            my $wd = cwd;
            $src = $wd . "/" . $src;
            symlink( $src, $conf ) or croak "Symlink failed: $!";
            say "\tSymlinking: " . $src . " -> " . $conf;
        }
    }
}
__END__
